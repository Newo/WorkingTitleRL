# Introduction

Greetings to anybody who find this page.

This is the source for a roguelike I'm making using [this tutorial](http://rogueliketutorials.com/libtcod/1) to get me started.

I'm also taking part in the dev-along at [r/roguelikedev.](https://www.reddit.com/r/roguelikedev/comments/8s5x5n/roguelikedev_does_the_complete_roguelike_tutorial/?sort=new)