import libtcodpy as libtcod

from game_messages import Message
from game_states import GameStates
from render_functions import RenderOrder


def kill_player(player):
    player.char = '%'
    player.color = libtcod.dark_red

    return Message('You died!', libtcod.red), GameStates.PLAYER_DEAD


def kill_monster(monster):
    death_message = Message('{0} is dead!'.format(monster.name.capitalize()), libtcod.orange)

    # If the monster has flesh (e.g. a zombie) make the corpse look like this.
    if monster.flesh:
        monster.char = '%'
        monster.color = libtcod.dark_red
        monster.blocks = False
        monster.fighter = None
        monster.ai = None
        monster.name = 'remains of ' + monster.name
        monster.render_order = RenderOrder.CORPSE
    # If the monster is just bones (e.g. a skeleton) make the corpse look like this.
    else:
        monster.char = '%'
        monster.color = libtcod.white
        monster.blocks = False
        monster.fighter = None
        monster.ai = None
        monster.name = 'Pile of ' + monster.name + ' bones'
        monster.render_order = RenderOrder.CORPSE

    return death_message
